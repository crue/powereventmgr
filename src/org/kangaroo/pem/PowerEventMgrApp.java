package org.kangaroo.pem;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.util.Timer;
import java.util.TimerTask;

import lombok.Cleanup;
import lombok.val;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;
import org.androidannotations.annotations.SystemService;
import org.kangaroo.pem.activity.ForceSleepActivity_;
import org.kangaroo.pem.activity.MainActivity_;
import org.kangaroo.pem.activity.PreferencesActivity_;
import org.kangaroo.pem.activity.SleepActivity_;
import org.kangaroo.pem.utils.AudioFocus;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.RemoteViews;

@EApplication
public class PowerEventMgrApp extends Application {

    private static final String TAG = PowerEventMgrApp.class.getSimpleName();

    private static final int MAIN_NOTIFICATION = 1;

    @SystemService PowerManager pm;
    @SystemService NotificationManager nm;

    @Bean AudioFocus audioFocus;

    private SharedPreferences prefs;

    private static WakeLock wakeLock = null;

    @Override
    public void onCreate() {

        super.onCreate();

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public void showNotification() {

        val prefs = PreferenceManager.getDefaultSharedPreferences(this);

        val views = new RemoteViews(getPackageName(), R.layout.notification);

        val builder = new Notification.Builder(this);

        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setPriority(-1);

        builder.setSmallIcon(prefs.getBoolean("enable_status_bar_icon", true) ? R.drawable.ic_launcher : android.R.color.transparent);

        builder.setContentIntent(PendingIntent.getActivity(this, 0, MainActivity_.intent(this)
                .action(Intent.ACTION_MAIN).get(), PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContent(views);

        views.setOnClickPendingIntent(R.id.notifySettingsBtn, PendingIntent.getActivity(this, 0, PreferencesActivity_.intent(this)
                .action(Intent.ACTION_MAIN).get(), PendingIntent.FLAG_UPDATE_CURRENT));

        if (prefs.getBoolean("skip_countdown", false)) {
            views.setOnClickPendingIntent(R.id.notifySleepBtn, PendingIntent.getActivity(this, 0, ForceSleepActivity_.intent(this)
                    .action(Intent.ACTION_MAIN)
                    .flags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
                    .extra("startService", true)
                    .get(), PendingIntent.FLAG_UPDATE_CURRENT));
        } else {
            views.setOnClickPendingIntent(R.id.notifySleepBtn, PendingIntent.getActivity(this, 0, SleepActivity_.intent(this)
                    .action(Intent.ACTION_MAIN)
                    .flags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
                    .extra("startService", true)
                    .get(), PendingIntent.FLAG_UPDATE_CURRENT));
        }

        nm.notify(MAIN_NOTIFICATION, builder.build());
    }

    public void hideNotification() {

        nm.cancel(MAIN_NOTIFICATION);
    }

    @SuppressWarnings("deprecation")
    private WakeLock getWakeLock() {

        if (wakeLock == null) {
            wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "org.kangaroo.pem.screenLock");
        }
        return wakeLock;
    }

    public boolean getFIMode() {

        return "1".equals(fileReadOneLine("/sys/kernel/usbhost/usbhost_fixed_install_mode"));
    }

    public boolean getHostMode() {

        return "1".equals(fileReadOneLine("/sys/kernel/usbhost/usbhost_hostmode"));
    }

    public boolean getExtenalPower() {

        return "1".equals(fileReadOneLine("/sys/kernel/usbhost/usbhost_external_power"));
    }

    public String fileReadOneLine(String fname) {

        try {
            val file = new File(fname);
            if (file.exists()) {
                @Cleanup
                val br = new BufferedReader(new FileReader(file));
                return br.readLine().trim();
            }
        } catch (Exception e) {
            Log.e(TAG, "IO Exception when reading " + fname + " file", e);
        }
        return null;
    }

    public boolean readSettingsFile(String fname) {

        return "1".equals(fileReadOneLine("/sys/kernel/usbhost/" + fname));
    }

    public void writeSettingsFile(String fname, boolean enable) {

        Process proc = null;
        try {
            proc = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(proc.getOutputStream());
            os.writeBytes("echo " + (enable ? 1 : 0) + " > /sys/kernel/usbhost/" + fname + "\n");
            os.flush();
            os.writeBytes("exit\n");
            os.flush();
            proc.waitFor();
        } catch (Throwable t) {
            Log.e(TAG, "Writing settings file failed", t);
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }
    }

    public void unlockScreen() {

        if (prefs.getBoolean("lock_screen", false)) {
            val lock = getWakeLock();
            if (lock.isHeld()) {
                lock.release();
            }
        }
    }

    public void wakeUp() {

        final Editor editor = prefs.edit();
        Timer timer = new Timer();

        if (getFIMode() && prefs.getBoolean("lock_screen", false)) {
            timer.schedule(new TimerTask() {

                @Override
                public void run() {

                    getWakeLock().acquire();
                }
            }, 1000);
        }

        if (prefs.getBoolean("flag_audiofocus", false)) {
            timer.schedule(new TimerTask() {

                @Override
                public void run() {

                    try {
                        editor.remove("flag_audiofocus").apply();
                        audioFocus.releaseFocus();
                    } catch (Exception e) {
                        Log.e(TAG, "failed to releas audio focus", e);
                    }
                }
            }, prefs.getInt("audio_focus_timeout", 3) * 1000);
        }

        if (prefs.getBoolean("flag_airplanemode", false)) {
            timer.schedule(new TimerTask() {

                @Override
                public void run() {

                    editor.remove("flag_airplanemode").apply();
                    Log.i(TAG, "deactivate airplane mode");
                    setAirPlaneMode(false);
                }
            }, 1000);
        }

        timer.schedule(new TimerTask() {

            @Override
            public void run() {

                editor.putBoolean("flag_hostmode", getHostMode()).apply();
            }
        }, 500);
    }

    public void goToSleep() {

        final Editor editor = prefs.edit();

        editor
                .remove("flag_audiofocus")
                .remove("flag_airplanemode")
                .apply();

        try {
            audioFocus.grabFocus();
            editor.putBoolean("flag_audiofocus", true).apply();
        } catch (Exception e) {
            Log.e(TAG, "failed to grab audio focus", e);
        }

        if (Settings.Global.getInt(getContentResolver(), "airplane_mode_on", 0) == 0) {
            Log.i(TAG, "activate airplane mode");
            setAirPlaneMode(true);
            editor.putBoolean("flag_airplanemode", true).apply();
        } else {
            Log.i(TAG, "airplane mode was already active");
        }

        Log.i(TAG, "PowerManager goToSleep");
        sleep();
    }

    private void setAirPlaneMode(boolean state) {

        Process proc = null;
        try {
            proc = Runtime.getRuntime().exec("su");
            val os = new DataOutputStream(proc.getOutputStream());
            os.writeBytes("settings put global airplane_mode_on " + (state ? "1" : "0") + "\n");
            os.flush();
            os.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state " + state + "\n");
            os.flush();
            os.writeBytes("exit\n");
            os.flush();
            proc.waitFor();
        } catch (Throwable t) {
            Log.e(TAG, "Setting of airplane_mode_on " + state + " failed", t);
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }
    }

    private void sleep() {

        Process proc = null;
        try {
            proc = Runtime.getRuntime().exec("su");
            val os = new DataOutputStream(proc.getOutputStream());
            os.writeBytes("input keyevent POWER\n");
            os.flush();
            os.writeBytes("exit\n");
            os.flush();
            proc.waitFor();
        } catch (Throwable t) {
            Log.e(TAG, "Going to sleep failed", t);
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }
    }

    public void startIntentSleep(boolean screenService) {

        val builder = SleepActivity_.intent(this);
        builder.action(Intent.ACTION_MAIN);
        builder.flags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        if (screenService) {
            builder.extra("startService", true);
        }

        builder.start();
    }

    public void startIntentForceSleep(boolean screenService) {

        val builder = ForceSleepActivity_.intent(this);
        builder.action(Intent.ACTION_MAIN);
        builder.flags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        if (screenService) {
            builder.extra("startService", true);
        }

        builder.start();
    }

}
