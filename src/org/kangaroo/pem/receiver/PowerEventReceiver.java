package org.kangaroo.pem.receiver;

import lombok.val;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EReceiver;
import org.kangaroo.pem.PowerEventMgrApp;
import org.kangaroo.pem.activity.PluginSettingsActivity;
import org.kangaroo.pem.utils.BundleScrubber;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

@EReceiver
public class PowerEventReceiver extends BroadcastReceiver {

    private static final String TAG = PowerEventReceiver.class.getSimpleName();

    @App PowerEventMgrApp app;

    public void onReceive(Context context, Intent intent) {

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String intentAction = intent.getAction();

        if (com.twofortyfouram.locale.Intent.ACTION_FIRE_SETTING.equals(intentAction)) {
            Log.i(TAG, "FIRE_SETTING");
            BundleScrubber.scrub(intent);
            if (intent.hasExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE)) {
                val bundle = intent.getBundleExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE);
                BundleScrubber.scrub(bundle);
                if (bundle.containsKey(PluginSettingsActivity.BUNDLE_EXTRA_SKIP_COUNTDOWN)) {
                    app.unlockScreen();
                    if (bundle.getBoolean(PluginSettingsActivity.BUNDLE_EXTRA_SKIP_COUNTDOWN)) {
                        app.startIntentForceSleep(true);
                    } else {
                        app.startIntentSleep(true);
                    }
                }
            }
            return;
        }

        if (Intent.ACTION_BOOT_COMPLETED.equals(intentAction)) {
            Log.i(TAG, "BOOT_COMPLETED");

            if (prefs.getBoolean("enable_notification", true)) {
                app.showNotification();
            }

            app.wakeUp();

            app.writeSettingsFile("usbhost_fixed_install_mode", prefs.getBoolean("fixed_install_mode", false));
            app.writeSettingsFile("usbhost_fastcharge_in_host_mode", prefs.getBoolean("fastcharge_in_host_mode", false));
            app.writeSettingsFile("usbhost_firm_sleep", prefs.getBoolean("firm_sleep", false));
            app.writeSettingsFile("usbhost_lock_usbdisk", prefs.getBoolean("lock_usbdisk", false));

            return;
        }

        if (Intent.ACTION_POWER_CONNECTED.equals(intentAction)) {
            Log.i(TAG, "ACTION_POWER_CONNECTED");

            app.wakeUp();

            return;
        }

        if (Intent.ACTION_POWER_DISCONNECTED.equals(intentAction)) {
            Log.i(TAG, "ACTION_POWER_DISCONNECTED");

            app.unlockScreen();

            if (app.getFIMode()) {

                if (!prefs.getBoolean("flag_hostmode", false)) {
                    Log.i(TAG, "abort - not in USB host mode");
                    return;
                }

                if (SystemClock.elapsedRealtime() < 20000) {
                    Log.i(TAG, "abort - just booted");
                    return;
                }

                if (prefs.getBoolean("skip_countdown", false)) {
                    app.startIntentForceSleep(false);
                } else {
                    app.startIntentSleep(false);
                }
            }

            return;
        }

        Log.w(TAG, "unknown intentAction=" + intentAction);
    }
}
