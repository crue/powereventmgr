package org.kangaroo.pem.preference;

import org.kangaroo.pem.R;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

public class TimoutPreference extends DialogPreference {

    private int timeout;
    private NumberPicker timeoutMin;
    private NumberPicker timeoutSec;

    public TimoutPreference(Context context, AttributeSet attrs) {

        super(context, attrs);

        setDialogLayoutResource(R.layout.timeout_preference);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {

        return a.getInt(index, 0);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

        setValue(restoreValue ? getPersistedInt(timeout) : (Integer) defaultValue);
    }

    @Override
    public CharSequence getSummary() {

        return getMinutes(timeout) + " min. " + getSeconds(timeout) + " sec.";
    }

    public int getValue() {

        return timeout;
    }

    public void setValue(int value) {

        value = Math.max(0, value);
        if (shouldPersist()) {
            persistInt(value);
        }
        if (value != timeout) {
            timeout = value;
            notifyChanged();
        }
    }

    @Override
    protected View onCreateDialogView() {

        View view = super.onCreateDialogView();

        timeoutMin = (NumberPicker) view.findViewById(R.id.timeoutMin);
        timeoutSec = (NumberPicker) view.findViewById(R.id.timeoutSec);

        timeoutMin.setMinValue(0);
        timeoutMin.setMaxValue(60);
        timeoutMin.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        timeoutMin.setValue(getMinutes(timeout));

        timeoutSec.setMinValue(0);
        timeoutSec.setMaxValue(59);
        timeoutSec.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        timeoutSec.setValue(getSeconds(timeout));

        return view;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        if (which == Dialog.BUTTON_POSITIVE) {
            setValue(timeoutMin.getValue() * 60 + timeoutSec.getValue());
        }
    }

    private int getMinutes(int time) {

        return (int) Math.floor(time / 60);
    }

    private int getSeconds(int time) {

        return time % 60;
    }

}
