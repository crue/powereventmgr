package org.kangaroo.pem.activity;

import lombok.val;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.kangaroo.pem.PowerEventMgrApp;
import org.kangaroo.pem.R;
import org.kangaroo.pem.service.ScreenService_;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

@EActivity(R.layout.main)
public class MainActivity extends Activity {

    protected static boolean IN_TASK_STACK = false;

    @App PowerEventMgrApp app;

    @ViewById Button btnSleep;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        IN_TASK_STACK = true;

        val prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (prefs.getBoolean("enable_notification", true)) {
            app.showNotification();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        val button = menu.add(R.string.app_settings);
        button.setIcon(R.drawable.ic_sysbar_quicksettings);
        button.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        button.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            public boolean onMenuItemClick(MenuItem item) {

                PreferencesActivity_.intent(MainActivity.this).start();
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @AfterViews
    void onInit() {

        if (app.getExtenalPower()) {
            btnSleep.setVisibility(View.GONE);
        }
    }

    @Click(R.id.btnSleep)
    void onSleepClick() {

        ScreenService_.intent(this).start();

        app.goToSleep();

        finish();
    }

    @Override
    protected void onDestroy() {

        IN_TASK_STACK = false;

        super.onDestroy();
    }

}
