package org.kangaroo.pem.activity;

import lombok.val;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.kangaroo.pem.R;
import org.kangaroo.pem.utils.BundleScrubber;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

@EActivity(R.layout.tasker)
public class PluginSettingsActivity extends Activity {

    public static final String BUNDLE_EXTRA_SKIP_COUNTDOWN = "org.kangaroo.pem.app.extra.SKIP_COUNTDOWN";

    private boolean isCancelled;

    @ViewById CheckBox cbSkipCountdown;

    @AfterViews
    void onInit() {

        isCancelled = true;

        BundleScrubber.scrub(getIntent());

        if (getIntent().hasExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE)) {
            val bundle = getIntent().getBundleExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE);

            BundleScrubber.scrub(bundle);

            if (bundle.containsKey(PluginSettingsActivity.BUNDLE_EXTRA_SKIP_COUNTDOWN)) {
                cbSkipCountdown.setChecked(bundle.getBoolean(PluginSettingsActivity.BUNDLE_EXTRA_SKIP_COUNTDOWN));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        val button = menu.add(R.string.app_settings);
        button.setIcon(android.R.drawable.ic_menu_save);
        button.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        button.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            public boolean onMenuItemClick(MenuItem item) {

                isCancelled = false;
                finish();
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void finish()
    {

        if (isCancelled)
        {
            setResult(RESULT_CANCELED);
        }
        else
        {
            val intent = new Intent();

            if (cbSkipCountdown.isChecked()) {
                val result = new Bundle();
                result.putBoolean(BUNDLE_EXTRA_SKIP_COUNTDOWN, true);
                intent.putExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE, result);
                intent.putExtra(com.twofortyfouram.locale.Intent.EXTRA_STRING_BLURB, "Sleep: skip countdown");
            } else {
                val result = new Bundle();
                result.putBoolean(BUNDLE_EXTRA_SKIP_COUNTDOWN, false);
                intent.putExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE, result);
                intent.putExtra(com.twofortyfouram.locale.Intent.EXTRA_STRING_BLURB, "Sleep: show countdown");
            }

            setResult(RESULT_OK, intent);
        }

        super.finish();
    }

}
