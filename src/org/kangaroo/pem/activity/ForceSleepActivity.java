package org.kangaroo.pem.activity;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.kangaroo.pem.PowerEventMgrApp;
import org.kangaroo.pem.service.ScreenService_;

import android.app.Activity;
import android.os.Bundle;

@EActivity
public class ForceSleepActivity extends Activity {

    @App PowerEventMgrApp app;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        app.goToSleep();

        if (getIntent().hasExtra("startService")) {
            ScreenService_.intent(this).start();
        }

        finish();
    }

}
