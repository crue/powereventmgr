package org.kangaroo.pem.activity;

import java.util.Timer;
import java.util.TimerTask;

import lombok.Cleanup;
import lombok.val;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.UiThread.Propagation;
import org.androidannotations.annotations.ViewById;
import org.kangaroo.pem.PowerEventMgrApp;
import org.kangaroo.pem.R;
import org.kangaroo.pem.service.ScreenService_;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

@EActivity(R.layout.sleep)
public class SleepActivity extends Activity {

    private static final String TAG = SleepActivity.class.getSimpleName();

    private final Timer timer = new Timer();

    @App PowerEventMgrApp app;

    @ViewById TextView tvCountdown;
    @ViewById ImageView imageBackground;
    @ViewById VideoView videoBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @AfterViews
    void onInit() {

        if (getIntent().hasExtra("startService")) {
            ScreenService_.intent(this).start();
        }

        val prefs = PreferenceManager.getDefaultSharedPreferences(this);

        tvCountdown.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AtomicClockRadio.ttf"));

        val type = prefs.getString("sleep_background_type", null);
        if (type != null) {
            if (type.startsWith("image/")) {
                try {
                    @Cleanup
                    val in = openFileInput("sleepscreen");
                    val bm = BitmapFactory.decodeStream(in);
                    imageBackground.setVisibility(View.VISIBLE);
                    imageBackground.setImageBitmap(bm);
                } catch (Exception e) {
                    Log.e(TAG, "", e);
                }
            } else if (type.startsWith("video/")) {
                videoBackground.setOnCompletionListener(new OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {

                        app.goToSleep();
                        finish();
                    }
                });
                videoBackground.setVisibility(View.VISIBLE);
                videoBackground.setVideoPath(getFileStreamPath("sleepscreen").getAbsolutePath());
                videoBackground.start();
                return;
            }
        }

        final boolean deviceIsPowered = app.getExtenalPower();
        final long startTime = System.currentTimeMillis();
        final long timeout = prefs.getInt("before_sleep_timeout", 5) * 1000;

        timer.schedule(new TimerTask() {

            @Override
            public void run() {

                if (!deviceIsPowered && app.getExtenalPower()) {
                    timer.cancel();
                    finish();
                    return;
                }

                long timecheck = System.currentTimeMillis() - startTime;

                setCountdown((int) ((timeout - timecheck) / 1000));

                if (timecheck >= timeout) {
                    timer.cancel();

                    app.goToSleep();

                    finish();
                }
            }
        }, 0, 1000);
    }

    @UiThread(propagation = Propagation.REUSE)
    void setCountdown(int sec) {

        tvCountdown.setText("" + sec);
    }

    @Override
    protected void onDestroy() {

        timer.cancel();
        super.onDestroy();
    }

}
