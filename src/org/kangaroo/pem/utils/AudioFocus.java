package org.kangaroo.pem.utils;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SystemService;

import android.media.AudioManager;
import android.util.Log;

@EBean
public class AudioFocus implements AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = AudioFocus.class.getSimpleName();

    @SystemService AudioManager am;

    public void grabFocus() {

        int result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

        Log.i(TAG, "grab focus result=" + (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED ? "granted" : "failed"));
    }

    public void releaseFocus() {

        int result = am.abandonAudioFocus(this);

        Log.i(TAG, "release focus result=" + (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED ? "granted" : "failed"));
    }

    public void onAudioFocusChange(int focusChange) {

        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                Log.i(TAG, "onAudioFocusChange AUDIOFOCUS_GAIN");
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                Log.i(TAG, "onAudioFocusChange AUDIOFOCUS_LOSS");
                break;
            default:
                Log.i(TAG, "onAudioFocusChange focusChange=" + focusChange);
                break;
        }
    }

}
